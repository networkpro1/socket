#Client

# Import socket module
import socket

# Create a socket object
s = socket.socket()

# Define the port on which you want to connect
port = 12345

# connect to the server on local computer
s.connect(('127.0.0.1', port))

# receive data from the server and decoding to get the string.
print (s.recv(1024).decode())

while True:
    n = int(s.recv(1024).decode())
    if n > 100:
        break
    print(n)
    s.send(str(n+1).encode())
# close the connection
s.close()